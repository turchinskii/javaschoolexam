package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {
    
    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     *
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (isPossible(inputNumbers)) {
            Collections.sort(inputNumbers);
            return fillPyramid(inputNumbers);
        } else {
            throw new CannotBuildPyramidException();
        }
    }
    
    private int[][] fillPyramid(final List<Integer> inputNumbers) {
        final int numberOfLayers = (int) (0.5 * (Math.sqrt(8 * inputNumbers.size() + 1) - 1));
        final int numberOfColumns = (numberOfLayers - 1) * 2 + 1;
        int[][] pyramid = new int[numberOfLayers][numberOfColumns];
        final Iterator<Integer> integerIterator = inputNumbers.iterator();
        for (int i = 0; i < numberOfLayers; i++) {
            for (int j = numberOfLayers - 1 - i; j <= numberOfColumns - (numberOfLayers - i); j += 2) {
                pyramid[i][j] = integerIterator.next();
            }
        }
        return pyramid;
    }
    
    private boolean isPossible(final List<Integer> inputNumbers) {
        return ((0.5 * (Math.sqrt(8 * inputNumbers.size() + 1) - 1)) % 1 == 0) && !inputNumbers.contains(null);
    }
    
}
