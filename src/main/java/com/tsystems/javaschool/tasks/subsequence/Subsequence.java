package com.tsystems.javaschool.tasks.subsequence;

import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class Subsequence {
    
    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
        if (!x.isEmpty() && y.isEmpty()) {
            return false;
        }
        if (x.isEmpty()) {
            return true;
        }
        int count = countCommon(x, y);
        return count == x.size();
    }
    
    private int countCommon(List<Object> x, List<Object> y) {
        final Iterator<Object> xIterator = x.iterator();
        final Iterator<Object> yIterator = y.iterator();
        int count = 0;
        Object xElem = null;
        Object yElem = null;
        if (xIterator.hasNext()) {
            xElem = xIterator.next();
        }
        boolean match = false;
        int maxlength = Math.max(x.size(), y.size());
        for (int i = 0; i < maxlength; i++) {
            if (match && xIterator.hasNext()) {
                xElem = xIterator.next();
                match = false;
            }
            if (yIterator.hasNext()) {
                yElem = yIterator.next();
            }
            if (Objects.equals(xElem, yElem)) {
                match = true;
                count++;
            }
        }
        return count;
    }
    
}
