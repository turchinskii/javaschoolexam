package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicInteger;

public class Calculator {
    
    final HashMap<String, Boolean> status = new HashMap<>();
    final HashMap<String, Integer> priority = new HashMap<>();
    final AtomicInteger index = new AtomicInteger(0);
    private final Stack<Double> numbers = new Stack<>();
    private final Stack<String> operations = new Stack<>();
    
    private static boolean isNumeric(final String str) {
        return str.matches("\\d+(\\.\\d+)?");
    }
    
    private static boolean isOperation(final String str) {
        return str.matches("[*+/-]");
    }
    
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        String evaluating = statement;
        if (evaluating == null || evaluating.isEmpty()) {
            return null;
        }
        evaluating = evaluating.trim();
        
        status.put("number", false);
        status.put("operation", false);
        
        priority.put("(", 3);
        priority.put(")", 3);
        priority.put("*", 2);
        priority.put("/", 2);
        priority.put("+", 1);
        priority.put("-", 1);
        
        final Stack<String> parenthesis = new Stack<>();
        if (parseString(evaluating, parenthesis)) {
            return null;
        }
    
        if (!parenthesis.isEmpty()) {
            return null;
        }
    
        double operationResult = 0;
        
        while (!operations.isEmpty()) {
            if (numbers.size() < 2) {
                return null;
            }
            final double y = numbers.pop();
            if (y == 0 && "/".equals(operations.peek())) {
                return null;
            }
            final double x = numbers.pop();
            operationResult = performOperation(x, y, operations.pop());
            numbers.push(operationResult);
        }
        if (operationResult % 1 == 0) {
            return Integer.toString((int) operationResult);
        } else {
            final BigDecimal bigDecimal = new BigDecimal(operationResult);
            
            if (bigDecimal.scale() > 4) {
                final BigDecimal bigDecimal1 = bigDecimal.setScale(4, RoundingMode.HALF_UP);
                operationResult = bigDecimal1.doubleValue();
            }
            return Double.toString(operationResult);
        }
    }
    
    private boolean parseString(String evaluating, Stack<String> parenthesis) {
        while (index.get() < evaluating.length()) {
            final String elem = getNextElement(evaluating, index, status);
            if (elem == null) {
                return true;
            }
            if (parseOperation(priority, elem)) {
                return true;
            }
            if (isNumeric(elem)) {
                numbers.push(Double.parseDouble(elem));
            }
            if (performOnOpenParenthesis(evaluating, parenthesis, elem)) {
                return true;
            }
            if (performOnCloseParenthesis(evaluating, parenthesis, elem)) {
                return true;
            }
            
        }
        return false;
    }
    
    private boolean parseOperation(HashMap<String, Integer> priority, String elem) {
        if ("*".equals(elem) || "/".equals(elem) || "+".equals(elem) || "-".equals(elem)) {
            while (!operations.isEmpty() && !"(".equals(operations.peek()) && priority.get(operations.peek()) >=
                                                                              (priority.get(elem))) {
                if (performOperation()) {
                    return true;
                }
            }
            operations.push(elem);
        }
        return false;
    }
    
    private boolean performOnCloseParenthesis(String evaluating, Stack<String> parenthesis, String elem) {
        if (")".equals(elem)) {
            if (parenthesis.isEmpty() || !"(".equals(parenthesis.pop()) ||
                !isNumeric(evaluating.substring(index.get() - 2, index.get() - 1)) && evaluating.charAt(
                        index.get() - 1) != ')' ||
                index.get() < evaluating.length() - 1 && (!isOperation(
                        evaluating.substring(index.get(), index.get() + 1)) && evaluating.charAt(index.get()) !=
                                                                               ')')) {
                return true;
            }
            if (evaluateUntilOpenParenthesis()) {
                return true;
            }
            if (!operations.isEmpty() && "(".equals(operations.peek())) {
                operations.pop();
            } else {
                return true;
            }
        }
        return false;
    }
    
    private boolean evaluateUntilOpenParenthesis() {
        while (!operations.isEmpty() && !"(".equals(operations.peek())) {
            if (performOperation()) {
                return true;
            }
        }
        return false;
    }
    
    private boolean performOperation() {
        if (numbers.size() < 2) {
            return true;
        }
        final double y = numbers.pop();
        
        if (y == 0 && "/".equals(operations.peek())) {
            return true;
        }
        
        final double x = numbers.pop();
        final double operationResult = performOperation(x, y, operations.pop());
        numbers.push(operationResult);
        return false;
    }
    
    private boolean performOnOpenParenthesis(String evaluating, Stack<String> parenthesis, String elem) {
        if ("(".equals(elem)) {
            if (index.get() > 1 && !isOperation(evaluating.substring(index.get() - 2, index.get() - 1)) &&
                evaluating.charAt(index.get() - 1) != '(' ||
                index.get() < evaluating.length() - 1 && (!isNumeric(
                        evaluating.substring(index.get(), index.get() + 1)) &&
                                                          evaluating.charAt(index.get()) != '(')) {
                return true;
            }
            operations.push(elem);
            parenthesis.push(elem);
        }
        return false;
    }
    
    private double performOperation(final double x, final double y, final String elem) {
        double result = 0;
        switch (elem) {
            case "*":
                result = x * y;
                break;
            case "/":
                result = x / y;
                break;
            case "+":
                result = x + y;
                break;
            case "-":
                result = x - y;
                break;
        }
        return result;
    }
    
    private String getNextElement(
            final String statement, final AtomicInteger index,
            final HashMap<String, Boolean> state) {
        final StringBuilder stringBuilder = new StringBuilder();
        final char currentChar = statement.charAt(index.get());
        if (currentChar == '(') {
            stringBuilder.append(currentChar);
            index.incrementAndGet();
            return stringBuilder.toString();
        }
        
        if (currentChar == ')') {
            stringBuilder.append(currentChar);
            index.incrementAndGet();
            return stringBuilder.toString();
        }
        if (!Character.toString(currentChar).matches("[\\d*+/-]")) {
            return null;
        }
        
        if (currentChar == '*' || currentChar == '/' ||
            currentChar == '+' || currentChar == '-') {
            if (state.get("operation")) {
                return null;
            }
            stringBuilder.append(currentChar);
            index.incrementAndGet();
            state.put("operation", true);
            state.put("number", false);
            return stringBuilder.toString();
        }
        
        if (isNumeric(statement.substring(index.get(), index.get() + 1))) {
            if (state.get("number")) {
                return null;
            }
            boolean isDecimal = false;
            int increase = 0;
            //int tmp = 0;
            while (index.get() + increase < statement.length() &&
                   isNumeric(statement.substring(index.get(), index.get() + increase + 1))) {
                if (statement.length() > index.get() + increase + 1 &&
                    statement.charAt(index.get() + increase + 1) == '.' &&
                    (statement.length() <= index.get() + increase + 2 ||
                     !isNumeric(statement.substring(index.get(), index.get() + increase + 3)))) {
                    return null;
                }
                if (statement.length() > index.get() + increase + 2 &&
                    statement.charAt(index.get() + increase + 1) == '.') {
                    if (isDecimal) {
                        return null;
                    } else {
                        increase += 1;
                        isDecimal = true;
                    }
                }
                increase++;
            }
            if (statement.charAt(index.get() + increase - 1) == '.' &&
                statement.charAt(index.get() + increase) == '.') {
                return null;
            }
            
            if (increase == 0) {
                increase++;
            }
    
            stringBuilder.append(statement, index.get(), index.get() + increase);
            index.set(index.get() + increase);
            state.put("number", true);
            state.put("operation", false);
            return stringBuilder.toString();
        }
        return "";
    }
    
}
